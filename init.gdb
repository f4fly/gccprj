target extended-remote :3333
layout split 
layout as
layout regs
fs n
fs n
file ./build/flyf4.elf
monitor reset
starti -y
set logging file ./gdb.tmp
set logging enabled on
define myquit
  # 在这里添加你想要在退出 GDB 之前执行的命令
  set logging enabled off
  quit
end